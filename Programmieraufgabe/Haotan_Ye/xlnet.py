from transformers import AutoModel, AutoTokenizer, XLNetModel, XLNetTokenizer
from transformers import XLNetForSequenceClassification, AdamW
from transformers import get_linear_schedule_with_warmup
from transformers import pipeline
from torch.utils.data import TensorDataset, DataLoader
import torch
import torch.nn as nn
import torch.nn.functional as F
import transformer_utils
import sys

device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
print("Using {} device".format(device))
PRETRAINED_MODEL_PATH = './XLNET_BASE_CASED'
MAXLEN = 30
BATCH_SIZE = 32


print('Loading pre-trained model from', PRETRAINED_MODEL_PATH)
tokenizer = XLNetTokenizer.from_pretrained('xlnet-base-cased')
model = XLNetModel.from_pretrained(PRETRAINED_MODEL_PATH)

train_dir = './TWEETS/EN_CLARIN_full/train'
dev_dir = './TWEETS/EN_CLARIN_full/dev'
test_dir = './TWEETS/EN_CLARIN_full/test'

train_dataloader = transformer_utils.create_dataloader(train_dir, tokenizer, MAXLEN, BATCH_SIZE, verbose=True)
dev_dataloader = transformer_utils.create_dataloader(dev_dir, tokenizer, MAXLEN, BATCH_SIZE, verbose=True)
test_dataloader = transformer_utils.create_dataloader(test_dir, tokenizer, MAXLEN, BATCH_SIZE, verbose=True)


class CLSAModel(nn.Module):
    def __init__(self, dropout_rate, num_classes):
        super(CLSAModel, self).__init__()
        self.xlnet = model
        self.dropout = nn.Dropout(dropout_rate)
        self.fc = nn.Linear(self.xlnet.config.hidden_size, num_classes)

    def forward(self, input_ids, attention_masks):
        last_hidden = self.xlnet(input_ids, attention_masks).last_hidden_state # [BATCH * MAXLEN(+2) * HIDDEN(768)]
        output = last_hidden[:, -1] # extract hidden rep of <cls>'s [BATCH * HIDDEN(768)]
        out = self.dropout(output)
        out = self.fc(out)
        out = F.softmax(out, dim=1)
        return out

twnet = CLSAModel(dropout_rate=0.2, num_classes=3) # 116720643 param
twnet.to(device)


EPOCHS = 6
optimizer = AdamW(model.parameters(), lr=2e-5, correct_bias=False)
total_steps = len(train_dataloader) * EPOCHS
scheduler = get_linear_schedule_with_warmup(optimizer, num_warmup_steps=0, num_training_steps=total_steps)
loss_fn = nn.CrossEntropyLoss().to(device)

transformer_utils.train_phase(twnet, train_dataloader, dev_dataloader, loss_fn, optimizer, device, scheduler, EPOCHS)

twnet = torch.load('best_model.pt')
print('* restored best weights from best_model.pt')
transformer_utils.test_phase(twnet, device, test_dataloader)
