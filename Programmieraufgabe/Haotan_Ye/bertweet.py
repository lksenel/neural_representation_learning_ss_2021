from transformers import AutoModel, AutoTokenizer, XLNetModel, XLNetTokenizer
from transformers import AutoModelForSequenceClassification, XLNetForSequenceClassification, AdamW
from transformers import get_linear_schedule_with_warmup
from transformers import pipeline
from torch.utils.data import TensorDataset, DataLoader
import torch
import torch.nn as nn
import torch.nn.functional as F
import transformer_utils

tokenizer = AutoTokenizer.from_pretrained('finiteautomata/bertweet-base-sentiment-analysis')
model = AutoModel.from_pretrained('finiteautomata/bertweet-base-sentiment-analysis')

device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
print("Using {} device".format(device))

MAXLEN = 30
BATCH_SIZE = 32

train_dir = './TWEETS/CLEAN/EN_CLARIN_full/train'
dev_dir = './TWEETS/CLEAN/EN_CLARIN_full/dev'
test_dir = './TWEETS/CLEAN/EN_CLARIN_full/test'
train_dataloader = transformer_utils.create_dataloader(train_dir, tokenizer, MAXLEN, BATCH_SIZE, verbose=True)
dev_dataloader = transformer_utils.create_dataloader(dev_dir, tokenizer, MAXLEN, BATCH_SIZE, verbose=True)
test_dataloader = transformer_utils.create_dataloader(test_dir, tokenizer, MAXLEN, BATCH_SIZE, verbose=True)

class CLSAModel(nn.Module):
    def __init__(self, dropout_rate, num_classes):
        super(CLSAModel, self).__init__()
        self.bert = model
        self.dropout = nn.Dropout(dropout_rate)
        self.fc = nn.Linear(self.bert.config.hidden_size, num_classes)

    def forward(self, input_ids, attention_masks):
        last_hidden, pooled_output = self.bert(input_ids, attention_masks, return_dict=False)
        # pooled_output = self.bert(input_ids, attention_masks, return_dict=False)[0] # uncomment if only evaluation
        # out = pooled_output # uncomment if only evaluation
        out = self.dropout(pooled_output) # comment if only evaluation
        out = self.fc(out) # comment if only evaluation
        out = F.softmax(out, dim=1)
        return out

twnet = CLSAModel(dropout_rate=0.2, num_classes=3)
twnet.to(device)


EPOCHS = 6
optimizer = AdamW(model.parameters(), lr=2e-5, correct_bias=False)
total_steps = len(train_dataloader) * EPOCHS
scheduler = get_linear_schedule_with_warmup(optimizer, num_warmup_steps=0, num_training_steps=total_steps)
loss_fn = nn.CrossEntropyLoss().to(device)

transformer_utils.train_phase(twnet, train_dataloader, dev_dataloader, loss_fn, optimizer, device, scheduler, EPOCHS)

print('* testing with last model')
transformer_utils.test_phase(twnet, device, test_dataloader)
twnet = torch.load('best_model.pt') # restore best weights
print('* restored best weights from best_model.pt')
transformer_utils.test_phase(twnet, device, test_dataloader)