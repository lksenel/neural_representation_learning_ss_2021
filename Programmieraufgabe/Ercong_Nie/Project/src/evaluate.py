#!/usr/bin/python3

from sentence_transformers import SentenceTransformer, util
from nltk.tokenize import sent_tokenize
from collections import defaultdict

class Evaluate():
    def __init__(self, model, questions, answers, truth_pool):
        self.model = model
        self.label2questions = questions
        self.label2answers = answers
        self.label2truth_pool = truth_pool
        self.total_num = len(self.label2questions)
        self.label2question_embedding = self.create_question_embedding()
        self.label2ans_embedding = self.create_ans_embedding()

    @classmethod
    def load_model(cls, model_name, questions, answers, truth_pool):
        ''' Load model from its name '''
        model = SentenceTransformer(model_name)
        return cls(model, questions, answers, truth_pool)

    def create_question_embedding(self):
        label2question_embedding = dict()
        for label, question in self.label2questions.items():
            label2question_embedding[label] = self.model.encode(question)
        return label2question_embedding

    def create_ans_embedding(self):
        label2ans_embedding = dict()
        for label, ans in self.label2answers.items():
            label2ans_embedding[label] = self.model.encode(ans)
        return label2ans_embedding

    def create_tokenized_ans_embedding(self):
        label2tokenized_ans_embedding = dict()
        for label, ans in self.label2answers.items():
            tokenized_ans = sent_tokenize(ans)
            tokenized_ans_embeddings = self.model.encode(tokenized_ans)
            tokenized_ans_embedding = tokenized_ans_embeddings.mean(0)
            label2tokenized_ans_embedding[label] = tokenized_ans_embedding
        return label2tokenized_ans_embedding


    def result(self):
        true_num = 0
        for question_label, question_embedding in self.label2question_embedding.items():
            truth, pool = self.label2truth_pool[question_label]
            answer_embeddings = [self.label2ans_embedding[label] for label in pool]
            # answer_embeddings = [self.label2tokenized_ans_embedding[label] for label in pool]
            cos_sim = util.cos_sim(question_embedding, answer_embeddings)
            index_cos_sim = enumerate(cos_sim[0])
            sorted_cos_sim = sorted(index_cos_sim, key=lambda x:x[1], reverse=True)

            best_answer_label = pool[sorted_cos_sim[0][0]]
            if best_answer_label in truth:
                true_num += 1

        acc = round(true_num/self.total_num, 4)
        return acc

    def result_tokenizing_answer(self, n):
        true_num = 0
        for question_label, question_embedding in self.label2question_embedding.items():
            truth, pool = self.label2truth_pool[question_label]
            sentences = dict()
            for ans_label in pool:
                ans = self.label2answers[ans_label]
                tokenized_ans = sent_tokenize(ans)
                for sent in tokenized_ans:
                    sentences[sent] = ans_label
            sentence_embeddings = self.model.encode(list(sentences))

            cos_sim = util.cos_sim(question_embedding, sentence_embeddings)
            index_cos_sim = enumerate(cos_sim[0])
            sorted_cos_sim = sorted(index_cos_sim, key=lambda x:x[1], reverse=True)
            n_best = sorted_cos_sim[:n] if n < len(sentences) else sorted_cos_sim
            n_best_sent = [idx for idx, _ in n_best]

            ans_sim = defaultdict(int)
            for idx in n_best_sent:
                ans_label = list(sentences.values())[idx]
                ans_sim[ans_label] += cos_sim[0][idx]

            sorted_ans_sim = sorted(ans_sim.items(), key=lambda d:d[1], reverse=True)
            best_ans_label = sorted_ans_sim[0][0]

            if best_ans_label in truth:
                true_num += 1

        acc = round(true_num / self.total_num, 4)
        return acc

    # def result_v2(self):
    #     true_num = 0
    #     for question_label, question_embedding in self.label2question_embedding.items():
    #         truth, pool = self.label2truth_pool[question_label]
    #         sentences = dict()
    #         for ans_label in pool:
    #             ans = self.label2answers[ans_label]
    #             tokenized_ans = sent_tokenize(ans)
    #             for sent in tokenized_ans:
    #                 sentences[sent] = ans_label
    #         sentence_embeddings = self.model.encode(list(sentences))









