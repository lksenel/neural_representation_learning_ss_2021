The deadline for *Hausarbeit* and *Programmieraufgabe* is by the end of August. 

If you choose to write a *Hausarbeit*, please send it to Prof. Schütze.

If you choose to do a *Programmieraufgabe*, please send it to Lütfi Kerem Senel (lksenel@gmail.com).