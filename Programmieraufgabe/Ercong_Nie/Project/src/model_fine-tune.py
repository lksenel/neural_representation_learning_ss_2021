from sentence_transformers import SentenceTransformer, losses, evaluation
from torch.utils.data import DataLoader
import pickle
import math

# Read the data
PATH_TO_TRAINING_DATA = 'F:/SS_2021/A3 Neural Representation Learning/Project/data/training_data'
PATH_TO_EVAL_DATA = 'F:/SS_2021/A3 Neural Representation Learning/Project/data/eval_data'
MODEL_NAME = 'paraphrase-TinyBERT-L6-v2'
MODEL_SAVE_PATH = 'fine_tune_model'
train_batch_size = 64
num_epochs = 1


# load training data and evaluation data
f_train_data = open(PATH_TO_TRAINING_DATA, 'rb')
train_data = pickle.load(f_train_data)
f_train_data.close()

f_eval_data = open(PATH_TO_EVAL_DATA, 'rb')
eval_data = pickle.load(f_eval_data)
f_eval_data.close()

# select base model, set training data and training loss function
model = SentenceTransformer(MODEL_NAME)
train_dataloader = DataLoader(train_data, shuffle=True, batch_size=train_batch_size)
train_loss = losses.CosineSimilarityLoss(model)

# set evaluator using eval data
evaluator = evaluation.EmbeddingSimilarityEvaluator.from_input_examples(eval_data)

# train the model
warmup_steps = math.ceil(len(train_data) * 0.1)
model.fit(train_objectives=[(train_dataloader, train_loss)],
          epochs=1,
          warmup_steps=warmup_steps,
          evaluator=evaluator,
          evaluation_steps=2000,
          output_path=MODEL_SAVE_PATH)
