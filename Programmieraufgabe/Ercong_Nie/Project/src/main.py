from evaluation_data import DataPreparation
from evaluate import Evaluate
import time

VOCABULARY_PATH = 'data/vocabulary.txt'
LABEL2ANSWER_PATH = 'data/InsuranceQA_label2answer.txt'
QUESTION_ANSLABEL_TRAIN_PATH = 'data/InsuranceQA_question_anslabel_train.txt'
QUESTION_ANSLABEL_TEST_PATH = 'data/InsuranceQA_question_anslabel_test.txt'
SAVE_RESULT_PATH = 'result_fine_tune'
# SAVE_RESULT_PATH = 'eval_result'


# MODEL_NAMES = ['paraphrase-MiniLM-L3-v2',
#                'paraphrase-MiniLM-L6-v2',
#                'paraphrase-TinyBERT-L6-v2',
#                'stsb-mpnet-base-v2',
#                'stsb-roberta-base-v2',
#                'average_word_embeddings_komninos',
#                'paraphrase-mpnet-base-v2',
#                'paraphrase-distilroberta-base-v2',
#                'paraphrase-MiniLM-L12-v2']


MODEL_NAMES= ['fine_tune_model']

dataset = DataPreparation(VOCABULARY_PATH, LABEL2ANSWER_PATH, QUESTION_ANSLABEL_TEST_PATH)


questions_test = dataset.label2question_test
truth_pool = dataset.label2truth_pool_test
answers = dataset.label2ans

with open(SAVE_RESULT_PATH,'w', encoding='utf-8') as f:

    for name in MODEL_NAMES:
        start = time.time()
        eval = Evaluate.load_model(name, questions_test, answers, truth_pool)
        print('Score of', name, ':', eval.result(), file=f)
        end = time.time()
        duration = end-start

        print('Running time is', round(duration,5), file=f)


