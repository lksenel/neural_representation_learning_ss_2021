import os
import torch
import torch.nn as nn
import numpy as np
from tqdm import tqdm
from sklearn.metrics import accuracy_score, f1_score
from torch.utils.data import TensorDataset, DataLoader


def create_dataloader(dir_path, tokenizer, max_len, batch_size, verbose=False):
    '''
    load [texts], [labels] from files
    create input_ids & attention_masks
    create TensorDataset & DataLoader
    returns dataloader{ each batch contains
        input -> shape [batch_size * maxlen(+2)]
        attention_masks -> shape [batch_size * maxlen(+2)]
        label tensor -> shape [batch_size]
        }
    '''
    if verbose:
        print('Loading data from', dir_path)
    texts, labels = load_data(dir_path)
    input_ids = []
    attention_masks = []
    for sent in texts:
        encoded_dict = tokenizer.encode_plus(
                        sent,
                        add_special_tokens = True, # '[CLS]' and '[SEP]'
                        max_length = max_len+2,
                        # pad_to_max_length = True, # deprecated, use following 2 lines
                        padding = 'max_length',
                        truncation = True,
                        return_attention_mask = True,
                        return_tensors = 'pt',
                        )
        input_ids.append(encoded_dict['input_ids'])
        attention_masks.append(encoded_dict['attention_mask'])
    input_ids = torch.cat(input_ids, dim=0)
    attention_masks = torch.cat(attention_masks, dim=0)
    labels = torch.tensor(labels)
    dataset = TensorDataset(input_ids, attention_masks, labels)
    dataloader = DataLoader(dataset, batch_size=batch_size, shuffle=True)
    return dataloader

def load_data(dir_path, verbose=False):
    texts = []
    labels = []
    for label_file in ['neg.tsv', 'neu.tsv', 'pos.tsv']:
        file_path = os.path.join(dir_path, label_file)
        if verbose:
            print('loading ' + file_path + '...')
        with open(file_path) as f:
            for line in f:
                id, polarity, text = [item.strip() for item in line.split('\t')]
                texts.append(text)
                if polarity.lower() == 'negative':
                    labels.append(0)
                elif polarity.lower() == 'neutral':
                    labels.append(1)
                elif polarity.lower() == 'positive':
                    labels.append(2)
    return texts, labels


def train_phase(model, train_dataloader, dev_dataloader, criterion, optimizer, device, scheduler, epochs):
        best_eval_loss = float('inf')
        for e in range(epochs):
            # TRAIN STEP
            model.train()
            epoch_total_train_loss = 0
            epoch_total_train_acc = 0
            with tqdm(train_dataloader, unit='batch') as train_epoch:
                train_epoch.set_description(f'Epoch [{e+1}/{epochs}]')
                for input_ids, attention_masks, labels in train_epoch:
                    input_ids, attention_masks, labels = input_ids.to(device), attention_masks.to(device), labels.to(device)
                    # optimizer.zero_grad()
                    output = model(input_ids, attention_masks)
                    train_loss = criterion(output, labels)
                    train_acc = accuracy_score(labels.detach().cpu().numpy(), output.detach().cpu().numpy().argmax(axis=1))
                    epoch_total_train_loss += train_loss.data.item()
                    epoch_total_train_acc += train_acc
                    train_loss.backward()
                    nn.utils.clip_grad_norm_(model.parameters(), max_norm=1.0)
                    optimizer.step()
                    scheduler.step()
                    optimizer.zero_grad()
            epoch_avg_train_loss = round(epoch_total_train_loss/len(train_dataloader), 4)
            epoch_avg_train_acc = round(epoch_total_train_acc/len(train_dataloader), 4)
            # VALIDATION STEP
            model.eval()
            epoch_total_eval_loss = 0
            epoch_total_eval_acc = 0
            with torch.no_grad():
                with tqdm(dev_dataloader, unit='batch') as dev_epoch:
                    dev_epoch.set_description('Validation')
                    for input_ids, attention_masks, labels in dev_epoch:
                        input_ids, attention_masks, labels = input_ids.to(device), attention_masks.to(device), labels.to(device)
                        pred = model(input_ids, attention_masks)
                        eval_loss = criterion(pred, labels)
                        eval_acc = accuracy_score(labels.detach().cpu().numpy(), pred.detach().cpu().numpy().argmax(axis=1))
                        epoch_total_eval_loss += eval_loss.data.item()
                        epoch_total_eval_acc += eval_acc
            epoch_avg_eval_loss = round(epoch_total_eval_loss/len(dev_dataloader), 4)
            epoch_avg_eval_acc = round(epoch_total_eval_acc/len(dev_dataloader), 4)
            if epoch_avg_eval_loss < best_eval_loss:
                best_eval_loss = epoch_avg_eval_loss
                print('* validation loss improved, saving model to best_model.pt')
                torch.save(model, 'best_model.pt')
            print('Train Loss:', epoch_avg_train_loss, '\tTrain Acc:', epoch_avg_train_acc, '\tValidation Loss:', epoch_avg_eval_loss, '\tValidation Acc:', epoch_avg_eval_acc, '\n')

def test_phase(model, device, test_dataloader, lang2_test_dataloader=None):
        model.eval()
        total_en_micro = 0
        total_en_macro = 0
        total_lang2_micro = 0
        total_lang2_macro = 0
        with torch.no_grad():
            with tqdm(test_dataloader, unit='batch') as test_epoch:
                test_epoch.set_description('Test evaluation')
                for input_ids, attention_masks, labels in test_epoch:
                    input_ids, attention_masks, labels = input_ids.to(device), attention_masks.to(device), labels.to(device)
                    pred = model(input_ids, attention_masks)
                    en_micro = round(f1_score(labels.detach().cpu().numpy(), pred.detach().cpu().numpy().argmax(axis=1), average='micro'), 4)
                    en_macro = round(f1_score(labels.detach().cpu().numpy(), pred.detach().cpu().numpy().argmax(axis=1), average='macro'), 4)
                    total_en_micro += en_micro
                    total_en_macro += en_macro
            if lang2_test_dataloader:
                with tqdm(lang2_test_dataloader, unit='batch') as test_epoch:
                    test_epoch.set_description('L2 test evaluation')
                    for input_ids, attention_masks, labels in test_epoch:
                        input_ids, attention_masks, labels = input_ids.to(device), attention_masks.to(device), labels.to(device)
                        pred = model(input_ids, attention_masks)
                        lang2_micro = round(f1_score(labels.detach().cpu().numpy(), pred.detach().cpu().numpy().argmax(axis=1), average='micro'), 4)
                        lang2_macro = round(f1_score(labels.detach().cpu().numpy(), pred.detach().cpu().numpy().argmax(axis=1), average='macro'), 4)
                        total_lang2_micro += lang2_micro
                        total_lang2_macro += lang2_macro
        avg_en_micro = round(total_en_micro/len(test_dataloader), 4)
        avg_en_macro = round(total_en_macro/len(test_dataloader), 4)
        if lang2_test_dataloader:
            avg_lang2_micro = round(total_lang2_micro/len(lang2_test_dataloader), 4)
            avg_lang2_macro = round(total_lang2_macro/len(lang2_test_dataloader), 4)
            print('{0: <10}'.format('En-micro') + '\t' + '{0: <10}'.format('L2-micro') + '\t' + '{0: <10}'.format('En-macro') + '\t' + '{0: <10}'.format('L2-macro'))
            print('{0: <10}'.format(avg_en_micro) + '\t' + '{0: <10}'.format(avg_lang2_micro) + '\t' + '{0: <10}'.format(avg_en_macro) + '\t' + '{0: <10}'.format(avg_lang2_macro))
        else:
            print('{0: <10}'.format('En-micro') + '\t' + '{0: <10}'.format('En-macro'))
            print('{0: <10}'.format(avg_en_micro) + '\t' + '{0: <10}'.format(avg_en_macro))
