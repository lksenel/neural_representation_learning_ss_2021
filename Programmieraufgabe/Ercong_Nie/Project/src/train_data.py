#!/usr/bin/python3

from sentence_transformers import InputExample
import random
import pickle

VOCABULARY_PATH = 'data/vocabulary.txt'
LABEL2ANSWER_PATH = 'data/InsuranceQA_label2answer.txt'
QUESTION_ANSLABEL_TRAIN_PATH = 'data/InsuranceQA_question_anslabel_train.txt'
NUMBER_OF_EVALUATION_NUMBERS = 1000

# create the vocabulary dictionary (idx -> word)
vocabulary = dict()
with open(VOCABULARY_PATH, 'r', encoding='utf-8') as f:
    for line in f:
        idx, word = line.split('\t')
        vocabulary[idx] = word


def idx2sentence(sentence_idx):
    sentence_list = list()
    for idx in sentence_idx.split():
        if idx in vocabulary:
            sentence_list.append(vocabulary[idx].strip())  # remove line break after the word
    sentence = ' '.join(sentence_list)
    return sentence


# create the answer dictionary (label -> answer)
label2ans = dict()
with open(LABEL2ANSWER_PATH,'r',encoding='utf-8') as f:
    for line in f:
        label, ans_idx = line.split('\t')
        label2ans[label] = idx2sentence(ans_idx)

# create label2question dictionary and label2(truth & pool) dictionary
label2question = dict()
label2truth_pool = dict()
label = 1
with open(QUESTION_ANSLABEL_TRAIN_PATH, 'r', encoding='utf-8') as f:
    for line in f:
        _, question_idx, truth, pool = line.split('\t')
        label2question[str(label)] = idx2sentence(question_idx)
        label2truth_pool[str(label)] = (truth.split(), pool.split())
        label += 1

# create training data set
training_data = list()
for label in label2question:
    question = label2question[label]
    truth, pool = label2truth_pool[label]
    false_answers = list(set(pool)-set(truth))
    for i in range(len(truth)):
        true_answer = label2ans[truth[i]]
        false_answer = label2ans[random.choice(false_answers)]
        # randomly choose a false answer from the pool

        training_data.append(InputExample(texts=[question, true_answer], label=float(1)))
        # set the label of pair(question, true_answer) as 1

        training_data.append(InputExample(texts=[question, false_answer], label=float(0)))
        # set the label of pair(question, false_answer) as 0


# create evaluation data set for the evaluator during the fine-tuning
eval_data = random.sample(training_data, NUMBER_OF_EVALUATION_NUMBERS)
# randomly choose n questions from training set as evaluation data.

# update train data by excluding evaluation data
training_data = list(set(training_data)-set(eval_data))


# save training data and evaluation data
f_train_data = open('data/training_data','wb')
pickle.dump(training_data, f_train_data)
f_train_data.close()

f_eval_data = open('data/eval_data2','wb')
pickle.dump(eval_data, f_eval_data)
f_eval_data.close()