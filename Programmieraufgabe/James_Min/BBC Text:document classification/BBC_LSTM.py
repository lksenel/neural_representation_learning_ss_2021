import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import re, os
from sklearn.preprocessing import LabelEncoder
from sklearn import model_selection




PATH = '/Users/jamesmin/Documents/BBC/archive/bbc-fulltext (document classification)/bbc'

def readFile(full_file_name):
    with open(full_file_name, 'r', encoding="utf-8", errors='ignore') as f:
        return '\n'.join(f.readlines())
#retreives data from the PATH and put it list 'data' in the format of 'category' and 'text'
def getData():

    data = []
    
    for root, dirs, files in os.walk(PATH):
        for f in files:
            if os.path.splitext(f)[1] == '.txt':
                full_file_name = os.path.join(root, f)
                category = os.path.basename(root)
                data.append({'category':category,'text':readFile(full_file_name)})
                    
    return data

data_frame = pd.DataFrame(getData())

# cleans 'text' of any unnessary content
def clean_str(string):
    """
    Tokenization/string cleaning for all datasets except for SST.
    Original taken from https://github.com/yoonkim/CNN_sentence/blob/master/process_data.py
    """
    string = re.sub(r"[^A-Za-z0-9(),!?\'\`]", " ", string)
    string = re.sub(r"\'s", " \'s", string)
    string = re.sub(r"\'ve", " \'ve", string)
    string = re.sub(r"n\'t", " n\'t", string)
    string = re.sub(r"\'re", " \'re", string)
    string = re.sub(r"\'d", " \'d", string)
    string = re.sub(r"\'ll", " \'ll", string)
    string = re.sub(r",", " , ", string)
    string = re.sub(r"!", " ! ", string)
    string = re.sub(r"\(", " \( ", string)
    string = re.sub(r"\)", " \) ", string)
    string = re.sub(r"\?", " \? ", string)
    string = re.sub(r"\s{2,}", " ", string)
    return string.strip().lower()

data_frame['cleaned_text'] = data_frame['text'].apply(lambda x: clean_str(x))

from tensorflow import keras
from keras import layers
from keras import losses
from keras import utils
from keras.layers.experimental.preprocessing import TextVectorization
from keras.callbacks import EarlyStopping
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.layers import Dense, Embedding, LSTM, SpatialDropout1D, Bidirectional, Dropout

max_features = 6433     # the maximum number of words to keep, based on word frequency
tokenizer = Tokenizer(num_words=max_features )
tokenizer.fit_on_texts(data_frame['cleaned_text'].values)
X = tokenizer.texts_to_sequences(data_frame['cleaned_text'].values)
X = pad_sequences(X, padding = 'post', maxlen = 6433 )
Y = pd.get_dummies(data_frame['category']).values

from sklearn import model_selection


X_train, X_test, Y_train, Y_test = model_selection.train_test_split(X,Y, test_size = 0.20, random_state = 42,stratify = Y)

embid_dim = 300
lstm_out = 32


model = keras.Sequential()
model.add(Embedding(max_features, embid_dim, input_length = X.shape[1] ))
model.add(Bidirectional(LSTM(lstm_out)))
model.add(Dropout(0.4))
model.add(Dense(32, activation = 'relu'))
model.add(Dropout(0.4))
model.add(Dense(5,activation = 'softmax'))

batch_size = 128
earlystop = EarlyStopping(monitor='loss', min_delta=0, patience=3, verbose=0, mode='auto')
model.compile(loss = 'categorical_crossentropy', optimizer='adam',metrics = ['accuracy'])
history = model.fit(X_train, Y_train, epochs = 10, batch_size=batch_size, verbose = 1, validation_data= (X_test, Y_test),callbacks=[earlystop])

