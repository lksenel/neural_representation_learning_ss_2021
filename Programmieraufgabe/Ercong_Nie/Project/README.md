# Sentence Transformers for QA

This project combines the Sentence Trans-former and QA task. 
It fine-tuned a SentenceTransformer Model and applied the model to the QA task. 
For that, an evaluation method was designed to introduce Sentence Transformer model by sentence 
matching and semantic similarity. When selecting the model to be fine-tuned, 
we compared the performance of different pre-trained Sentence Trans-former models on solving QA tasks.

## data
`InsuranceQA_label2answer.txt`, `InsuranceQA_question_anslabel_test.txt`, `InsuranceQA_question_anslabel_train.txt`
and `vocabulary.txt` are the data sets.
`training_data` and `eval_data` are created by the program used for fine-tuning.

## fine_tune_model
This is the fine-tuned model.

## src
* `train_data.py` is used to create the training data for fine-tuning.
* `evaluation_data.py` is used to pre-process the corpus and serve for evaluation of QA task.
* `evaluae.py` uses the evaluation data to measure the accuracy of different Sentence Transformers.
* `model_fine-tune.py` is used to fine-tune the model.
* `main.py` combines all modules to create the final results.

## results
The results of pre-trained models are stored in `eval_result`. 
The result of the fine-tuned model is stored in `result_fine_tune`.