# BERT and XLNet fine-tuning for Twitter sentiment analysis

## Summary
* This programming project compares
    * a BERT and an XLNet model (`base` in both cases, to each other as well as a PyTorch model serving as the baseline)
    * two different sizes of the XLNet model (`base` and `large`)
* All comparisons are done by fine-tuning on Twitter data to solve sentiment analysis

## Data collection
* The tweets were downloaded using a toolkit (https://github.com/aritter/twitter_download) and tweet IDs provided by CLARIN (Mozetič et al.). Due to possible changes in privacy settings, not all tweets could be retrieved successfully, those no longer available were filtered. It was also noticed that some tweet IDs were repeated, which were also removed.
* The resulting dataset was split into train/dev/test sets with a ratio of 70/15/15

## Used models
* BiLSTM PyTorch model (baseline)
* bert-base-cased
* xlnet-base-cased
* xlnet-large-cased
* finiteautomata/bertweet-base-sentiment-analysis (fine-tuned)

## Training procedure
* For the PyTorch model, the entire model is trained.
* For BERT and XLNet models, fine-tuning is performed first, the model is saved if the validation loss improves.
* For BERTweet, the model is loaded in an already fine-tuned state and two settings are compared: without further fine-tuning (evaluation only) and additional fine-tuning with the same data used for BERT and XLNet models. The model is saved if the validation loss improves.

## Hyperparameter tuning
PyTorch (values chosen selectively)
* `MAXLEN` for padding/truncating of each tweet: ranges from 20 to 50
* `batch_size`: ranges from 16 to 128
* `embedding_dim`: 100, 200, 300
* `BiLSTM_dim`: ranges from 64 to 256
* `dropout`: ranges from 0.2 to 0.8
* `linear_dim`: 64, 128
* number of epochs: validation loss usually stops improving after 1-3 epochs
* results for most settings were quite similar, `MAXLEN=30` and `batch_size=32` seem to work best

Fine-tuning settings
* `dropout`: 0.2, 0.4, similar results
* number of epochs: tried 3 and 6, fine-tuning for 6 epochs works better for XLNet (about 3% F1 improvement), not so much different for BERT and BERTweet

## Evaluation
* Models were tested on the included test set using both the final checkpoint and the last saved checkpoint.
* For the PyTorch model, both checkpoints were usually 1-2 epochs apart, but did not show significant difference on the test results. Similar results were seen for the fine-tuning settings. This indicates that 1-2 epochs of additional training after convergence did not seem to have a large effect on the models' performance.

## Results
PyTorch
* micro-F1: 49.58, macro-F1: 32.55

BERT
* micro-F1: 67.09, macro-F1: 64.98

XLNet (base)
* When using `AutoTokenizer`, scores were much worse and did not beat the baseline, this was fixed after switching to the recommended `SentencePiece` tokenizer
* micro-F1: 65.69, macro-F1: 63.46
* (without `SentencePiece`) micro-F1: 47.76, macro-F1: 21.38

XLNet (large)
* Kept all settings from XLNet base, only the model was swapped. However, scores were unexpectedly low
* micro-F1: 47.80, macro-F1: 21.39

BERTweet (evaluation only)
* BERTweet uses the same architecture as BERT with the RoBERTa pre-training procedure. The implemented version comes pre-fine-tuned with about 15% more data as is used to fine-tuned the BERT and XLNet models.
* Scores are slightly higher than those obtained in the BERT experiment, both within 1 F1 score.
* micro-F1: 67.54, macro-F1: 65.91

BERTweet (fine-tuning + evaluation)
* Further fine-tuning is found beneficial
* micro-F1: 70.33, macro-F1: 68.88