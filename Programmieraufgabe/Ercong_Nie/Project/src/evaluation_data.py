#!/usr/bin/python3

class DataPreparation():
    def __init__(self,vocabulary_path, label2ans_path, question_anslabel_test_path):
        self.vocabulary = self.to_vocabulary(vocabulary_path)
        self.label2ans = self.to_label2ans(label2ans_path)
        self.label2question_test, self.label2truth_pool_test = self.to_question_truth_pool(question_anslabel_test_path)

    # transfer sentence in the form of index to words
    def idx2sentence(self, sentence_idx):
        vocabulary = self.vocabulary
        sentence_list = list()
        for idx in sentence_idx.split():
            if idx in vocabulary:
                sentence_list.append(vocabulary[idx].strip())  # remove line break after the word
        sentence = ' '.join(sentence_list)
        return sentence


    # create a dictionary to map indexes and words
    def to_vocabulary(self, vocabulary_path):
        vocabulary = dict()
        with open(vocabulary_path,'r',encoding='utf-8') as f:
            for line in f:
                idx, word = line.split('\t')
                vocabulary[idx] = word

        return vocabulary


    # create a dictionary to map labels and answers
    def to_label2ans(self,label2ans_path):
        label2ans = dict()
        with open(label2ans_path,'r',encoding='utf-8') as f:
            for line in f:
                label, ans_idx = line.split('\t')
                label2ans[label] = self.idx2sentence(ans_idx)

        return label2ans


    # create two dictionaries separately mapping label to questions and (ground_truth, pool)
    def to_question_truth_pool(self, question_anslabel_path):
        label2question = dict()
        label2truth_pool = dict()
        label = 1
        with open(question_anslabel_path, 'r', encoding='utf-8') as f:
            for line in f:
                _, question_idx, truth, pool = line.split('\t')
                label2question[str(label)] = self.idx2sentence(question_idx)
                label2truth_pool[str(label)] = (truth.split(),pool.split())
                label += 1

        return label2question, label2truth_pool